package by.shag.danilovich.service;

import by.shag.danilovich.api.dto.UserDto;
import by.shag.danilovich.jpa.model.User;
import by.shag.danilovich.jpa.repository.UserRepository;
import by.shag.danilovich.mapping.UserDtoMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDtoMapper mapper;

    @InjectMocks
    private UserService service;


    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, userRepository);
    }

    @Test
    void save() {

        User user = new User();
        user.setId(5);
        when(mapper.map(any(UserDto.class))).thenReturn(user);

        User saved = new User();
        saved.setName("Oleg");
        when(userRepository.save(any(User.class))).thenReturn(saved);

        UserDto dto = new UserDto();
        dto.setLogin("nagibator");
        when(mapper.map(any(User.class))).thenReturn(dto);

        UserDto dto1 = new UserDto();
        dto1.setLogin("Login");
        UserDto result = service.save(dto1);

        assertEquals(dto, result);

        verify(mapper).map(dto1);
        verify(userRepository).save(user);
        verify(mapper).map(saved);
    }
}