package by.shag.danilovich.mapping;

import by.shag.danilovich.api.dto.UserDto;
import by.shag.danilovich.jpa.model.User;

public class UserDtoMapper {

    public User map(UserDto dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setAge(dto.getAge());
        user.setName(dto.getName());
        user.setLastName(dto.getLastName());
        user.setLogin(dto.getLogin());
        return  user;
    }

    public UserDto map(User user) {
        UserDto dto = new UserDto();
        dto.setId(user.getId());
        dto.setAge(user.getAge());
        dto.setName(user.getName());
        dto.setLastName(user.getLastName());
        if (!user.getLogin().isEmpty() ) {
            user.setLogin(user.getLogin());
        } else {
            dto.setEmail(user.getLogin());
        }
        return dto;
    }
}
