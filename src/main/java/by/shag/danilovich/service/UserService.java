package by.shag.danilovich.service;

import by.shag.danilovich.api.dto.UserDto;
import by.shag.danilovich.jpa.model.User;
import by.shag.danilovich.jpa.repository.UserRepository;
import by.shag.danilovich.mapping.UserDtoMapper;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    private UserRepository userRepository;
    private UserDtoMapper mapper;

    public UserDto save(UserDto dto) {
        User user = mapper.map(dto);
        User saved = userRepository.save(user);
        return mapper.map(saved);
    }

    public List<UserDto> findByAll() {
        return userRepository.filByAll().stream()
                .map(user1 -> mapper.map(user1))
                .collect(Collectors.toList());
    }

    public UserDto filByID(Integer id) {
        User user = userRepository.filByID(id);
        return mapper.map(user);
    }

    public void deleteById(Integer id) {
        userRepository.deleteById(id);
    }

    public UserDto update(UserDto dto) {
        User user = mapper.map(dto);
        User opi = userRepository.update(user);
        return mapper.map(opi);
    }
}
