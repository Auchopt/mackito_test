package by.shag.danilovich.jpa.repository;

import by.shag.danilovich.jpa.model.User;

import java.util.List;

public class UserRepository {

/*    входит в сигнатуру: Название, список параметров, выбрасываемое исключение если есть */

    public User save(User user){
        throw new UnsupportedOperationException("save: " + user);
    }

    public List<User> filByAll() {
        throw new UnsupportedOperationException("Read ALL");
    }

    public User filByID(Integer id) {
        throw new UnsupportedOperationException("Read ID: " + id);
    }

    public User deleteById(Integer id) {
        throw new UnsupportedOperationException("Delete ID: " + id);
    }

    public User update(User user){
        throw new UnsupportedOperationException("Update: " + user);
    }
}
